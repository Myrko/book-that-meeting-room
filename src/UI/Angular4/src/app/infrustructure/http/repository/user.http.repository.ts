import { UserRepositoryInterface } from "../../../domain/user/repository/user.repository";
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { User } from "../../../domain/user/model/user";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class UserHttpRepository implements UserRepositoryInterface
{
    constructor(
        private http: Http
    ) {}

    findById(id: string): User {
        return new User('', '', '');
    }

    all(limit: number, offset: number): Observable<User[]> {
        let persons = this.http.get('http://localhost:8000/api/v1/person')
            .map((response: Response, t) => {
                return response.json().map((data) => {
                    return new User(
                        data.id,
                        data.name,
                        data.position
                    );
                });
            })
            .catch((error: any) => Observable.throw(error.json().error || "Server error"));

        return persons;
    }
}