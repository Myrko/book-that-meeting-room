import { Component } from '@angular/core';
import { Injectable } from '@angular/core';
import {UserHttpRepository} from "./infrustructure/http/repository/user.http.repository";
import {Observable} from "rxjs/Observable";
import {User} from "./domain/user/model/user";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

@Injectable()
export class AppComponent {
  title = 'app';

  private users: User[];

  constructor(
      private userRepository: UserHttpRepository
  ) {
      userRepository.all(1, 1).subscribe(
          (users) => this.users = users
      );
  }
}
