import {User} from "../model/user";
import {Observable} from "rxjs/Observable";

export interface UserRepositoryInterface
{
    findById(id: string): User;

    all(limit: number, offset: number): Observable<User[]>;
}