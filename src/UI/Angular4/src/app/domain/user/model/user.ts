export class User
{
    constructor(
        private id: string,
        private name: string,
        private position: string
    ) {}

    getId(): string
    {
        return this.id;
    }

    getName(): string
    {
        return this.name;
    }

    getPosition(): string
    {
        return this.position;
    }
}