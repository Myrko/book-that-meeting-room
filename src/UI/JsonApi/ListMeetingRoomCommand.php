<?php

namespace App\UI\JsonApi;

use App\Domain\Booking\Repository\BookingRepositoryInterface;
use App\Domain\MeetingRoom\Aggregate\MeetingRoom;
use App\Domain\MeetingRoom\Repository\MeetingRoomRepositoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ListMeetingRoomCommand
{
    public function __invoke(
        Request $request,
        MeetingRoomRepositoryInterface $meetingRoomRepository
    ): Response {
        /** @var MeetingRoom[] $meetingRooms */
        $meetingRooms = $meetingRoomRepository->all();

        $responseData = [];
        foreach ($meetingRooms as $meetingRoom) {
            $responseData[(string)$meetingRoom->id()] = [
                'id'    => (string) $meetingRoom->id(),
                'alias' => $meetingRoom->alias(),
            ];
        }

        return new JsonResponse(
            json_encode($responseData, JSON_PRETTY_PRINT),
            Response::HTTP_OK,
            [],
            true
        );
    }
}
