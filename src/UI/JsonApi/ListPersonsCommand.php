<?php

namespace App\UI\JsonApi;


use App\Domain\Person\Aggregate\Person;
use App\Domain\Person\Repository\PersonRepositoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ListPersonsCommand
{
    public function __invoke(
        Request $request,
        PersonRepositoryInterface $personRepository
    ): Response {

        /** @var Person[] $persons */
        $persons = $personRepository
            ->all();

        $responseData = [];
        foreach ($persons as $person) {
            $responseData[] = [
                'id'    => (string) $person->id(),
                'name' => $person->name(),
                'position' => $person->position()
            ];
        }

        return new JsonResponse(
            json_encode($responseData, JSON_PRETTY_PRINT),
            Response::HTTP_OK,
            [],
            true
        );
    }
}
