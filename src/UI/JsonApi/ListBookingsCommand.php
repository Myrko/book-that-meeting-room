<?php

namespace App\UI\JsonApi;

use App\Domain\Booking\Aggregate\Booking;
use App\Domain\Booking\Repository\BookingRepositoryInterface;
use App\Domain\MeetingRoom\Aggregate\MeetingRoom;
use App\Domain\MeetingRoom\Repository\MeetingRoomRepositoryInterface;
use App\Domain\MeetingRoom\ValueObject\MeetingRoomId;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ListBookingsCommand
{
    public function __invoke(
        Request $request,
        MeetingRoomRepositoryInterface $meetingRoomRepository,
        BookingRepositoryInterface $bookingRepository
    ): Response {
        $meetingRoomId = new MeetingRoomId($request->get('id'));
        $meetingRoom = $meetingRoomRepository->findByAggregateId($meetingRoomId);

        if (!$meetingRoom) {
            throw new \InvalidArgumentException('There is no room with id: ' . (string)$meetingRoomId);
        }

        $date = $request->get('date') ?
            new \DateTime($request->get('date')) : new \DateTime();

        /** @var Booking[] $bookings */
        $bookings = $bookingRepository->findByDay($meetingRoom, $date);

        $responseData = [];

        foreach ($bookings as $booking) {
            $responseData[(string)$booking->id()] = [
                'id'                => (string)$booking->id(),
                'person'            => [
                    'name'     => $booking->person()->name(),
                    'position' => $booking->person()->position(),
                ],
                'reservationPeriod' => [
                    'from' => $booking->reservationPeriod()->from(),
                    'to'   => $booking->reservationPeriod()->to(),
                ],
                'meetingRoom'       => [
                    'id'    => (string)$booking->meetingRoom()->id(),
                    'alias' => $booking->meetingRoom()->alias(),
                ],
            ];
        }

        return new JsonResponse(
            json_encode($responseData, JSON_PRETTY_PRINT),
            Response::HTTP_OK,
            [],
            true
        );
    }
}
