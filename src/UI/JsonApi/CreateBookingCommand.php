<?php

namespace App\UI\JsonApi;

use App\Application\Command\Request\BookMeetingRoomRequest;
use League\Tactician\CommandBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CreateBookingCommand
{
    public function __invoke(Request $request, CommandBus $commandBus): Response
    {
        $bookMeetingRoomRequest = new BookMeetingRoomRequest(
            $request->get('name'),
            $request->get('position'),
            $request->get('meetingRoomId'),
            new \DateTime($request->get('from')),
            new \DateTime($request->get('to'))
        );

        $id = $commandBus->handle($bookMeetingRoomRequest);

        return new JsonResponse(
            json_encode(['id' => (string) $id], JSON_PRETTY_PRINT),
            Response::HTTP_CREATED,
            [],
            true
        );
    }
}
