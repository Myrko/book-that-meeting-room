<?php

namespace App\UI\JsonApi;

use App\Application\Command\Request\AddMeetingRoomRequest;
use League\Tactician\CommandBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AddMeetingRoomCommand
{
    public function __invoke(Request $request, CommandBus $commandBus): Response
    {
        $addMeetingRoomRequest = new AddMeetingRoomRequest(
            $request->get('alias')
        );

        $id = $commandBus->handle($addMeetingRoomRequest);

        return new JsonResponse(
            json_encode(['id' => (string) $id], JSON_PRETTY_PRINT),
            Response::HTTP_CREATED,
            [],
            true
        );
    }
}
