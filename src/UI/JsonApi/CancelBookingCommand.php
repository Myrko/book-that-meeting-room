<?php

namespace App\UI\JsonApi;

use App\Application\Command\Request\BookMeetingRoomRequest;
use App\Application\Command\Request\UnBookMeetingRoomRequest;
use League\Tactician\CommandBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CancelBookingCommand
{
    public function __invoke(Request $request, CommandBus $commandBus): Response
    {
        $unBookMeetingRoomRequest = new UnBookMeetingRoomRequest(
            $request->get('id')
        );

        $commandBus->handle($unBookMeetingRoomRequest);

        return new JsonResponse(
            json_encode([], JSON_PRETTY_PRINT),
            Response::HTTP_NO_CONTENT,
            [],
            true
        );
    }
}
