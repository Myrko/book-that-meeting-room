<?php

namespace App\UI\Console;

use App\Application\Command\Request\AddPersonRequest;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AddPersonCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('person:add')
            ->addArgument('name', InputArgument::REQUIRED)
            ->addArgument('position', InputArgument::REQUIRED)
            ->setDescription('Adds a new person to booking system.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $request = new AddPersonRequest(
            $input->getArgument('name'),
            $input->getArgument('position')
        );

        $result = $this->getContainer()->get('tactician.commandbus')->handle($request);

        $output
            ->writeln('<info>' .$result . '</info>');
    }
}
