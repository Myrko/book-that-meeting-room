<?php

namespace App\UI\Console;

use App\Application\Command\Request\BookMeetingRoomRequest;
use App\Domain\Booking\Repository\BookingRepositoryInterface;
use App\Domain\Booking\ValueObject\ReservationPeriod;
use App\Domain\MeetingRoom\Repository\MeetingRoomRepositoryInterface;
use App\Domain\MeetingRoom\ValueObject\MeetingRoomId;
use App\Domain\Person\Aggregate\Person;
use App\Domain\Person\Repository\PersonRepositoryInterface;
use App\Domain\Person\ValueObject\PersonId;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PersonBookMeetingRoomCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('person:room:book')
            ->addArgument('personId', InputArgument::REQUIRED)
            ->addArgument('room', InputArgument::REQUIRED)
            ->addArgument('from', InputArgument::REQUIRED)
            ->addArgument('to', InputArgument::REQUIRED)
            ->setDescription('Creates a new booking.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $personId = $input->getArgument('personId');

        /** @var PersonRepositoryInterface $personRepository */
        $personRepository = $this
            ->getContainer()->get('App\Domain\Person\Repository\PersonRepositoryInterface');

        $person = $personRepository
            ->findByAggregateId(PersonId::fromString($input->getArgument('personId')));

        if (!$person) {
            $output
                ->writeln(
                    '<error>' .
                    'There is not any registered person with this id: ' . $personId .
                    '</error>'
                );

            return;
        }

        $meetingRoomId = $input->getArgument('room');

        /** @var MeetingRoomRepositoryInterface $meetingRoomRepository */
        $meetingRoomRepository = $this
            ->getContainer()->get('App\Domain\MeetingRoom\Repository\MeetingRoomRepositoryInterface');

        $meetingRoom = $meetingRoomRepository
            ->findByAggregateId(MeetingRoomId::fromString($meetingRoomId));

        if (!$meetingRoom) {
            $output
                ->writeln(
                    '<error>' .
                    'There is not any registered meeting rooms with this id: ' . $personId .
                    '</error>'
                );

            return;
        }

        $reservationPeriod = new ReservationPeriod(
            new \DateTime($input->getArgument('from')),
            new \DateTime($input->getArgument('to'))
        );

        $booking = $person
            ->bookMeetingRoom(
                $meetingRoom,
                $reservationPeriod
            );

        /** @var BookingRepositoryInterface $bookingRepository */
        $bookingRepository = $this
            ->getContainer()->get('App\Domain\Booking\Repository\BookingRepositoryInterface');

        $bookingRepository
            ->add($booking);

        $output
            ->writeln('<info>' . (string) $booking->id() . '</info>');
    }
}
