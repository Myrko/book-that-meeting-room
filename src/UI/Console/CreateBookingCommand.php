<?php

namespace App\UI\Console;

use App\Application\Command\Request\BookMeetingRoomRequest;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateBookingCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('room:book')
            ->addArgument('room', InputArgument::REQUIRED)
            ->addArgument('name', InputArgument::REQUIRED)
            ->addArgument('position', InputArgument::REQUIRED)
            ->addArgument('from', InputArgument::REQUIRED)
            ->addArgument('to', InputArgument::REQUIRED)
            ->setDescription('Creates a new booking.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $request = new BookMeetingRoomRequest(
            $input->getArgument('name'),
            $input->getArgument('position'),
            $input->getArgument('room'),
            new \DateTime($input->getArgument('from')),
            new \DateTime($input->getArgument('to'))
        );

        $result = $this->getContainer()->get('tactician.commandbus')->handle($request);

        $output
            ->writeln('<info>' .$result . '</info>');
    }
}
