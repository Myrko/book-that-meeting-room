<?php

namespace App\UI\Console;

use App\Application\Command\Request\BookMeetingRoomRequest;
use App\Application\Command\Request\UnBookMeetingRoomRequest;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CancelBookingCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('room:unbook')
            ->addArgument('id', InputArgument::REQUIRED)
            ->setDescription('Cancel booking.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $request = new UnBookMeetingRoomRequest(
            $input->getArgument('id')
        );

        $result = $this->getContainer()->get('tactician.commandbus')->handle($request);

        $output
            ->writeln('<info>' .$result . '</info>');
    }
}
