<?php

namespace App\UI\Console;

use App\Application\Command\Request\AddMeetingRoomRequest;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AddMeetingRoomCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('room:add')
            ->addArgument('alias', InputArgument::REQUIRED)
            ->setDescription('Creates a new room.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $request = new AddMeetingRoomRequest(
            $input->getArgument('alias')
        );

        $result = $this->getContainer()->get('tactician.commandbus')->handle($request);

        $output
            ->writeln('<info>' .$result . '</info>');
    }
}
