<?php

namespace App\UI\Console;

use App\Application\Command\Request\AddMeetingRoomRequest;
use App\Domain\Booking\Aggregate\Booking;
use App\Domain\MeetingRoom\ValueObject\MeetingRoomId;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ListBookingsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('room:list:bookings')
            ->addArgument('room', InputArgument::REQUIRED)
            ->addArgument('date', InputArgument::OPTIONAL)
            ->setDescription('Display bookings per specified date.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $meetingRoomId = $input->getArgument('room');

        $meetingRoom = $this
            ->getContainer()
            ->get('App\Domain\MeetingRoom\Repository\MeetingRoomRepositoryInterface')
            ->findByAggregateId(new MeetingRoomId($meetingRoomId));

        if (!$meetingRoom) {
            $output
                ->writeln(
                    '<error>There is no room with provided id:' . $meetingRoomId . '</error>'
                );

            return;
        }

        $date = $input->getArgument('date') ? new \DateTime($input->getArgument('date')): new \DateTime();

        $bookings = $this
            ->getContainer()
            ->get('App\Domain\Booking\Repository\BookingRepositoryInterface')
            ->findByDay($meetingRoom, $date);

        /** @var Booking $booking */
        foreach ($bookings as $booking) {
            $output
                ->writeln(
                    '<info>' .
                    $booking->person()->name() .
                    $booking->person()->position() .
                    $booking->reservationPeriod()->from()->format('Y-m-d H:i:s') .
                    $booking->reservationPeriod()->to()->format('Y-m-d H:i:s') .
                    '</info>'
                );
        }

    }
}
