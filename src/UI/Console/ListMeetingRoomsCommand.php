<?php

namespace App\UI\Console;

use App\Application\Command\Request\AddMeetingRoomRequest;
use App\Domain\Booking\Aggregate\Booking;
use App\Domain\MeetingRoom\Aggregate\MeetingRoom;
use App\Domain\MeetingRoom\ValueObject\MeetingRoomId;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ListMeetingRoomsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('room:list')
            ->setDescription('Displays all available rooms.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $meetingRooms = $this
            ->getContainer()
            ->get('App\Domain\MeetingRoom\Repository\MeetingRoomRepositoryInterface')
            ->all();

        /** @var MeetingRoom $meetingRoom */
        foreach ($meetingRooms as $meetingRoom) {
            $output
                ->writeln(
                    '<info>' .
                    $meetingRoom->id() .
                    $meetingRoom->alias() .
                    '</info>'
                );
        }

    }
}
