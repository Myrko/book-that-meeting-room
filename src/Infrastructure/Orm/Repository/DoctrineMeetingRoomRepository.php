<?php

namespace App\Infrastructure\Orm\Repository;

use App\Domain\MeetingRoom\Aggregate\MeetingRoom;
use App\Domain\MeetingRoom\Repository\MeetingRoomRepositoryInterface;
use App\Domain\MeetingRoom\ValueObject\MeetingRoomId;
use Doctrine\ORM\EntityRepository;

class DoctrineMeetingRoomRepository extends EntityRepository implements MeetingRoomRepositoryInterface
{
    public function add(MeetingRoom $meetingRoom)
    {
        $this->getEntityManager()->persist($meetingRoom);
        $this->getEntityManager()->flush($meetingRoom);
    }

    public function findByAggregateId(MeetingRoomId $meetingRoomId): MeetingRoom
    {
        return $this->find((string) $meetingRoomId);
    }

    public function all(): array
    {
        return $this->findAll();
    }
}
