<?php

namespace App\Infrastructure\Orm\Repository;

use App\Domain\Person\Aggregate\Person;
use App\Domain\Person\Repository\PersonRepositoryInterface;
use App\Domain\Person\ValueObject\PersonId;
use Doctrine\ORM\EntityRepository;

class DoctrinePersonRepository extends EntityRepository implements PersonRepositoryInterface
{
    public function add(Person $person)
    {
        $this->getEntityManager()->persist($person);
        $this->getEntityManager()->flush($person);
    }

    public function findByAggregateId(PersonId $personId): ?Person
    {
        return $this->find((string) $personId);
    }

    public function all(int $limit = 10, int $offset = 0): array
    {
        return $this->findAll();
    }
}