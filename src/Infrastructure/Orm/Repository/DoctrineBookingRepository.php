<?php

namespace App\Infrastructure\Orm\Repository;

use App\Domain\Booking\Aggregate\Booking;
use App\Domain\Booking\Repository\BookingRepositoryInterface;
use App\Domain\Booking\ValueObject\BookingId;
use App\Domain\Booking\ValueObject\ReservationPeriod;
use App\Domain\MeetingRoom\Aggregate\MeetingRoom;
use Doctrine\ORM\EntityRepository;

class DoctrineBookingRepository extends EntityRepository implements BookingRepositoryInterface
{

    public function add(Booking $meetingRoomBooking)
    {
        $this->getEntityManager()->persist($meetingRoomBooking);
        $this->getEntityManager()->flush($meetingRoomBooking);
    }

    public function findByAggregateId(BookingId $meetingRoomBookingId): Booking
    {
        return $this->find((string)$meetingRoomBookingId);
    }

    public function findByDay(MeetingRoom $meetingRoom, \DateTimeInterface $dateTime): array
    {
        $reservationPeriod = new ReservationPeriod(
            new \DateTime($dateTime->format('Y-m-d') . ' 00:00:00'),
            new \DateTime($dateTime->format('Y-m-d') . ' 23:59:59')
        );

        $queryBuilder = $this->createQueryBuilder('mrb');

        $queryBuilder
            ->andWhere(
                $queryBuilder->expr()->andX(
                    $queryBuilder->expr()->eq('mrb.meetingRoom', ':meetingRoomId'),
                    $queryBuilder->expr()->gte('mrb.reservationPeriod.from', ':from'),
                    $queryBuilder->expr()->lte('mrb.reservationPeriod.to', ':to')
                )
            );

        $queryBuilder
            ->setParameters(
                [
                    'meetingRoomId' => (string) $meetingRoom->id(),
                    'from'          => $reservationPeriod->from(),
                    'to'            => $reservationPeriod->to(),
                ]
            );

        return $queryBuilder->getQuery()->getResult();
    }

    public function findByReservationPeriod(MeetingRoom $meetingRoom, ReservationPeriod $reservationPeriod): array
    {
        $queryBuilder = $this->createQueryBuilder('mrb');

        $queryBuilder
            ->orWhere(
                $queryBuilder->expr()->andX(
                    $queryBuilder->expr()->eq('mrb.meetingRoom', ':meetingRoomId'),
                    $queryBuilder->expr()->between(':from', 'mrb.reservationPeriod.from', 'mrb.reservationPeriod.to')
                )
            )
            ->orWhere(
                $queryBuilder->expr()->andX(
                    $queryBuilder->expr()->eq('mrb.meetingRoom', ':meetingRoomId'),
                    $queryBuilder->expr()->between(':to', 'mrb.reservationPeriod.from', 'mrb.reservationPeriod.to')
                )
            );

        $queryBuilder
            ->setParameters(
                [
                    'meetingRoomId' => (string)$meetingRoom->id(),
                    'from'          => $reservationPeriod->from(),
                    'to'            => $reservationPeriod->to(),
                ]
            );

        return $queryBuilder->getQuery()->getResult();
    }

    public function remove(Booking $meetingRoomBooking)
    {
        $this->getEntityManager()->remove($meetingRoomBooking);
        $this->getEntityManager()->flush();
    }
}
