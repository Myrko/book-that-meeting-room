<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170627180616 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE app_person (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', name VARCHAR(255) NOT NULL, position VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE app_meeting_broom_booking');
        $this->addSql('ALTER TABLE app_booking CHANGE reservation_period_from reservation_period_from DATETIME NOT NULL, CHANGE reservation_period_to reservation_period_to DATETIME NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE app_meeting_broom_booking (id CHAR(36) NOT NULL COLLATE utf8_unicode_ci COMMENT \'(DC2Type:guid)\', meeting_room_id CHAR(36) NOT NULL COLLATE utf8_unicode_ci, person_name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, person_position VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, reservation_period_from DATETIME NOT NULL, reservation_period_to DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE app_person');
        $this->addSql('ALTER TABLE app_booking CHANGE reservation_period_from reservation_period_from DATETIME NOT NULL, CHANGE reservation_period_to reservation_period_to DATETIME NOT NULL');
    }
}
