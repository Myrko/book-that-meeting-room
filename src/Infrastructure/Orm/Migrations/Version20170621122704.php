<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170621122704 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE app_booking (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', meeting_room_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', person_name VARCHAR(255) NOT NULL, person_position VARCHAR(255) NOT NULL, reservation_period_from DATETIME NOT NULL, reservation_period_to DATETIME NOT NULL, INDEX IDX_D516D93CCC5381E (meeting_room_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_meeting_room (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', alias VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE app_booking ADD CONSTRAINT FK_D516D93CCC5381E FOREIGN KEY (meeting_room_id) REFERENCES app_meeting_room (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE app_booking DROP FOREIGN KEY FK_D516D93CCC5381E');
        $this->addSql('DROP TABLE app_booking');
        $this->addSql('DROP TABLE app_meeting_room');
    }
}
