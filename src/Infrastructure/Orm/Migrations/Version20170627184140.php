<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170627184140 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE app_booking ADD person_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', DROP person_name, DROP person_position, CHANGE reservation_period_from reservation_period_from DATETIME NOT NULL, CHANGE reservation_period_to reservation_period_to DATETIME NOT NULL');
        $this->addSql('ALTER TABLE app_booking ADD CONSTRAINT FK_D516D93217BBB47 FOREIGN KEY (person_id) REFERENCES app_person (id)');
        $this->addSql('CREATE INDEX IDX_D516D93217BBB47 ON app_booking (person_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE app_booking DROP FOREIGN KEY FK_D516D93217BBB47');
        $this->addSql('DROP INDEX IDX_D516D93217BBB47 ON app_booking');
        $this->addSql('ALTER TABLE app_booking ADD person_name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD person_position VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, DROP person_id, CHANGE reservation_period_from reservation_period_from DATETIME NOT NULL, CHANGE reservation_period_to reservation_period_to DATETIME NOT NULL');
    }
}
