<?php

namespace App\Application\Command\Handler;

use App\Application\Command\Request\UnBookMeetingRoomRequest;
use App\Domain\Booking\Service\CancelBooking;
use App\Domain\Booking\ValueObject\BookingId;

final class UnBookMeetingRoom
{
    /**
     * @var CancelBooking
     */
    private $cancelMeetingRoomBooking;

    /**
     * UnBookMeetingRoom constructor.
     *
     * @param CancelBooking $cancelMeetingRoomBooking
     */
    public function __construct(CancelBooking $cancelMeetingRoomBooking)
    {
        $this->cancelMeetingRoomBooking = $cancelMeetingRoomBooking;
    }

    /**
     * @param UnBookMeetingRoomRequest $request
     *
     * @return bool
     */
    public function handle(UnBookMeetingRoomRequest $request)
    {
        $this->cancelMeetingRoomBooking->__invoke(new BookingId($request->id()));

        return true;
    }
}
