<?php

namespace App\Application\Command\Handler;

use App\Application\Command\Request\BookMeetingRoomRequest;
use App\Domain\Booking\Service\CreateBooking;
use App\Domain\Booking\ValueObject\Person;
use App\Domain\Booking\ValueObject\ReservationPeriod;

final class BookMeetingRoom
{
    /**
     * @var CreateBooking
     */
    private $createMeetingRoomBooking;

    public function __construct(CreateBooking $createMeetingRoomBooking)
    {
        $this->createMeetingRoomBooking = $createMeetingRoomBooking;
    }

    /**
     * @param BookMeetingRoomRequest $request
     *
     * @return string
     */
    public function handle(BookMeetingRoomRequest $request)
    {
        $person = new Person(
            $request->name(),
            $request->position()
        );

        $reservationPeriod = new ReservationPeriod(
            $request->from(),
            $request->to()
        );

        return $this->createMeetingRoomBooking->__invoke($request->meetingRoomId(), $person, $reservationPeriod)->id();
    }
}
