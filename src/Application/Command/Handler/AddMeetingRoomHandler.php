<?php

namespace App\Application\Command\Handler;

use App\Application\Command\Request\AddMeetingRoomRequest;
use App\Domain\MeetingRoom\Service\AddMeetingRoom;

class AddMeetingRoomHandler
{
    private $addMeetingRoom;

    public function __construct(AddMeetingRoom $addMeetingRoom)
    {
        $this->addMeetingRoom = $addMeetingRoom;
    }

    public function handle(AddMeetingRoomRequest $request)
    {
       return $this->addMeetingRoom->__invoke($request->alias());
    }
}
