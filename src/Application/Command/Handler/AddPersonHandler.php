<?php

namespace App\Application\Command\Handler;

use App\Application\Command\Request\AddPersonRequest;
use App\Domain\Person\Service\AddPerson;

class AddPersonHandler
{
    private $addPerson;

    public function __construct(AddPerson $addPerson)
    {
        $this->addPerson = $addPerson;
    }

    public function handle(AddPersonRequest $addPersonRequest)
    {
        return $this->addPerson->__invoke(
            $addPersonRequest->name(),
            $addPersonRequest->posiiton()
        );
    }

    public function process(AddPersonRequest $addPersonRequest)
    {

    }
}