<?php

namespace App\Application\Command\Request;

final class AddPersonRequest
{
    private $name;
    private $position;

    public function __construct(string $name, string $position)
    {
        $this->name = $name;
        $this->position = $position;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function posiiton(): string
    {
        return $this->position;
    }
}
