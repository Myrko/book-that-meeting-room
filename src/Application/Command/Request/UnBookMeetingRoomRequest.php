<?php

namespace App\Application\Command\Request;

/**
 * Class UnBookMeetingRoomRequest
 * @package App\Application\Command\Request
 */
final class UnBookMeetingRoomRequest
{
    /**
     * @var string
     */
    private $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function id()
    {
        return $this->id;
    }
}
