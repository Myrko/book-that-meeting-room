<?php

namespace App\Application\Command\Request;

/**
 * Class BookMeetingRoomRequest
 * @package App\Application\Command\Request
 */
final class BookMeetingRoomRequest
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $position;
    /**
     * @var string
     */
    private $meetingRoomId;
    /**
     * @var \DateTimeInterface
     */
    private $from;
    /**
     * @var \DateTimeInterface
     */
    private $to;

    /**
     * BookMeetingRoomRequest constructor.
     * @param string $name
     * @param string $position
     * @param string $meetingRoomId
     * @param \DateTimeInterface $from
     * @param \DateTimeInterface $to
     */
    public function __construct(
        string $name,
        string $position,
        string $meetingRoomId,
        \DateTimeInterface $from,
        \DateTimeInterface $to
    )
    {
        $this->name = $name;
        $this->position = $position;
        $this->meetingRoomId = $meetingRoomId;
        $this->from = $from;
        $this->to = $to;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function position(): string
    {
        return $this->position;
    }

    /**
     * @return string
     */
    public function meetingRoomId(): string
    {
        return $this->meetingRoomId;
    }

    /**
     * @return \DateTimeInterface
     */
    public function from(): \DateTimeInterface
    {
        return $this->from;
    }

    /**
     * @return \DateTimeInterface
     */
    public function to(): \DateTimeInterface
    {
        return $this->to;
    }
}
