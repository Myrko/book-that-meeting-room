<?php

namespace App\Application\Command\Request;

final class AddMeetingRoomRequest
{
    private $alias;

    public function __construct(string $alias)
    {
        $this->alias = $alias;
    }

    public function alias(): string
    {
        return $this->alias;
    }
}
