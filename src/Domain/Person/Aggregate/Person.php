<?php

namespace App\Domain\Person\Aggregate;

use App\Domain\Booking\Aggregate\Booking;
use App\Domain\Booking\ValueObject\ReservationPeriod;
use App\Domain\MeetingRoom\Aggregate\MeetingRoom;
use App\Domain\Person\ValueObject\PersonId;

class Person
{
    private $id;
    private $name;
    private $position;

    public static function new(string $name, string $position)
    {
        $self = new self();

        $self->id = new PersonId();
        $self->name = $name;
        $self->position = $position;

        return $self;
    }

    public function bookMeetingRoom(MeetingRoom $meetingRoom, ReservationPeriod $reservationPeriod): Booking
    {
        return Booking::byPerson($this, $meetingRoom, $reservationPeriod);
    }

    public function id(): ?PersonId
    {
        return (is_string($this->id)) ? new PersonId($this->id) : $this->id;
    }

    public function name(): ?string
    {
        return $this->name;
    }

    public function position(): ?string
    {
        return $this->position;
    }
}
