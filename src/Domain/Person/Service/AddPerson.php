<?php

namespace App\Domain\Person\Service;

use App\Domain\Person\Aggregate\Person;
use App\Domain\Person\Repository\PersonRepositoryInterface;

class AddPerson
{
    private $personRepository;

    public function __construct(PersonRepositoryInterface $personRepository)
    {
        $this->personRepository = $personRepository;
    }

    public function __invoke(string $name, string $position)
    {
        $person = Person::new($name, $position);

        $this->personRepository->add($person);

        return (string) $person->id();
    }
}