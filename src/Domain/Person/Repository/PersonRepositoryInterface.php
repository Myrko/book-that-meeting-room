<?php

namespace App\Domain\Person\Repository;

use App\Domain\Person\Aggregate\Person;
use App\Domain\Person\ValueObject\PersonId;

interface PersonRepositoryInterface
{
    public function add(Person $person);

    public function findByAggregateId(PersonId $personId): ?Person;

    public function all(int $limit = 10, int $offset = 0): array;
}
