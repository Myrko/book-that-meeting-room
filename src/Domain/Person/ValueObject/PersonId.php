<?php

namespace App\Domain\Person\ValueObject;

use App\Domain\Common\ValueObject\AggregateRootId;

class PersonId extends AggregateRootId
{
}
