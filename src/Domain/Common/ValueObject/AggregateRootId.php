<?php

namespace App\Domain\Common\ValueObject;

use Ramsey\Uuid\Uuid;

abstract class AggregateRootId
{
    protected $uuid;

    public function __construct(?string $id = null)
    {
        $this->uuid = Uuid::fromString($id ?: Uuid::uuid4())->toString();
    }

    public function equals(AggregateRootId $aggregateRootId): bool
    {
        return $this->uuid === $aggregateRootId->__toString();
    }

    public function bytes(): string
    {
        return Uuid::fromString($this->uuid)->getBytes();
    }

    public static function fromBytes(string $bytes): self
    {
        return new static(Uuid::fromBytes($bytes)->toString());
    }

    public static function toBytes(string $uid): string
    {
        return (new static($uid))->bytes();
    }

    public static function fromString(string $uid)
    {
        return (new static($uid));
    }

    public function __toString(): string
    {
        return (string) $this->uuid;
    }
}
