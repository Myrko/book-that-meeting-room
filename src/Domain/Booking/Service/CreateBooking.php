<?php

namespace App\Domain\Booking\Service;

use App\Domain\Booking\Aggregate\Booking;
use App\Domain\Booking\Exception\BookingOnNonExistedMeetingRoomException;
use App\Domain\Booking\Exception\BookingOnReservedPeriodException;
use App\Domain\Booking\Repository\BookingRepositoryInterface;
use App\Domain\Booking\ValueObject\Person;
use App\Domain\Booking\ValueObject\ReservationPeriod;
use App\Domain\MeetingRoom\Repository\MeetingRoomRepositoryInterface;
use App\Domain\MeetingRoom\ValueObject\MeetingRoomId;

class CreateBooking
{
    private $bookingRepository;
    private $meetingRoomRepository;

    public function __construct(
        BookingRepositoryInterface $bookingRepository,
        MeetingRoomRepositoryInterface $meetingRoomRepository
    ) {
        $this->bookingRepository = $bookingRepository;
        $this->meetingRoomRepository = $meetingRoomRepository;
    }

    public function __invoke(
        string $meetingRoomId,
        Person $person,
        ReservationPeriod $reservationPeriod
    ): Booking {

        $meetingRoom = $this->meetingRoomRepository->findByAggregateId(new MeetingRoomId($meetingRoomId));

        if (!$meetingRoom) {
            throw  new BookingOnNonExistedMeetingRoomException(
                'Can not book meeting room if it does not exist for id: ' . $meetingRoomId
            );
        }

        $meetingRoomBookings = $this->bookingRepository->findByReservationPeriod($meetingRoom, $reservationPeriod);

        if (!empty($meetingRoomBookings)) {
            throw new BookingOnReservedPeriodException(
                'Can not book room for period: ' .
                $reservationPeriod->from()->format('Y-m-d H:i:s') . ' - ' .
                $reservationPeriod->to()->format('Y-m-d H:i:s')
            );
        }

        $meetingRoomBooking = Booking::book(
            $meetingRoom,
            $person,
            $reservationPeriod
        );

        $this->bookingRepository->add($meetingRoomBooking);

        return $meetingRoomBooking;
    }
}
