<?php

namespace App\Domain\Booking\Service;

use App\Domain\Booking\Repository\BookingRepositoryInterface;
use App\Domain\Booking\ValueObject\BookingId;

class CancelBooking
{
    private $meetingRoomBookingRepository;

    public function __construct(BookingRepositoryInterface $meetingRoomBookingRepository)
    {
        $this->meetingRoomBookingRepository = $meetingRoomBookingRepository;
    }

    public function __invoke(BookingId $meetingRoomBookingId)
    {
        $meetingRoomBooking = $this->meetingRoomBookingRepository->findByAggregateId($meetingRoomBookingId);

        $this->meetingRoomBookingRepository->remove($meetingRoomBooking);
    }
}
