<?php

namespace App\Domain\Booking\ValueObject;

class ReservationPeriod
{
    private $from;
    private $to;

    public function __construct(\DateTimeInterface $from, \DateTimeInterface $to)
    {
        $this->from = $from;
        $this->to = $to;
    }

    public function from() : \DateTimeInterface
    {
        return $this->from;
    }

    public function to(): \DateTimeInterface
    {
        return $this->to;
    }
}