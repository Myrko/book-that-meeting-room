<?php

namespace App\Domain\Booking\ValueObject;

use App\Domain\Common\ValueObject\AggregateRootId;

class BookingId extends AggregateRootId
{
}
