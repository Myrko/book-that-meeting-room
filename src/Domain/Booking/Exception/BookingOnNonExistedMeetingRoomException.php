<?php

namespace App\Domain\Booking\Exception;

class BookingOnNonExistedMeetingRoomException extends \Exception
{
}
