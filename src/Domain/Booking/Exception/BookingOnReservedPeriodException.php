<?php

namespace App\Domain\Booking\Exception;

class BookingOnReservedPeriodException extends \Exception
{
}
