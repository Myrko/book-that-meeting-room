<?php

namespace App\Domain\Booking\Repository;

use App\Domain\Booking\Aggregate\Booking;
use App\Domain\Booking\ValueObject\BookingId;
use App\Domain\Booking\ValueObject\ReservationPeriod;
use App\Domain\MeetingRoom\Aggregate\MeetingRoom;

/**
 * @todo: split to read/write?
 */
interface BookingRepositoryInterface
{
    public function add(Booking $meetingRoomBooking);

    public function findByAggregateId(BookingId $meetingRoomBookingId): Booking;

    public function findByDay(MeetingRoom $meetingRoom, \DateTimeInterface $dateTime): array ;

    public function findByReservationPeriod(MeetingRoom $meetingRoom, ReservationPeriod $reservationPeriod): array;

    public function remove(Booking $meetingRoomBooking);
}
