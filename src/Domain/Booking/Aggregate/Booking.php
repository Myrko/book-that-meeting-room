<?php

namespace App\Domain\Booking\Aggregate;

use App\Domain\Common\ValueObject\AggregateRootId;
use App\Domain\Booking\ValueObject\BookingId;
use App\Domain\Booking\ValueObject\ReservationPeriod;
use App\Domain\MeetingRoom\Aggregate\MeetingRoom;
use App\Domain\Person\Aggregate\Person;

class Booking
{
    private $id;
    private $meetingRoom;
    private $person;
    private $reservationPeriod;

//    public static function book(
//        MeetingRoom $meetingRoom,
//        Person $person,
//        ReservationPeriod $reservationPeriod
//    ): Booking {
//
//        $self = new self();
//        $self->id = new BookingId();
//        $self->meetingRoom = $meetingRoom;
//        $self->person = $person;
//        $self->reservationPeriod = $reservationPeriod;
//
//        return $self;
//    }

    public static function byPerson(
        Person $person,
        MeetingRoom $meetingRoom,
        ReservationPeriod $reservationPeriod
    ): Booking {

        $self = new self();
        $self->id = new BookingId();
        $self->meetingRoom = $meetingRoom;
        $self->person = $person;
        $self->reservationPeriod = $reservationPeriod;

        return $self;
    }


    public function id(): ?BookingId
    {
        return (is_string($this->id)) ? new BookingId($this->id) : $this->id;
    }

    public function meetingRoom(): ?MeetingRoom
    {
        return $this->meetingRoom;
    }

    public function person(): ?Person
    {
        return $this->person;
    }

    public function reservationPeriod(): ?ReservationPeriod
    {
        return $this->reservationPeriod;
    }
}
