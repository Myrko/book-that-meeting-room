<?php

namespace App\Domain\MeetingRoom\Aggregate;

use App\Domain\MeetingRoom\ValueObject\MeetingRoomId;

class MeetingRoom
{
    private $id;
    private $alias;

    public static function new(string $alias): MeetingRoom
    {
        $self = new self();
        $self->id = new MeetingRoomId();
        $self->alias = $alias;

        return $self;
    }

    public function id(): ?MeetingRoomId
    {
        return (is_string($this->id)) ? new MeetingRoomId($this->id) : $this->id;
    }

    public function alias(): string
    {
        return $this->alias;
    }
}
