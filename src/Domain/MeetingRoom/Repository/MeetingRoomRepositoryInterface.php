<?php

namespace App\Domain\MeetingRoom\Repository;

use App\Domain\MeetingRoom\Aggregate\MeetingRoom;
use App\Domain\MeetingRoom\ValueObject\MeetingRoomId;

interface MeetingRoomRepositoryInterface
{
    public function add(MeetingRoom $meetingRoom);

    public function findByAggregateId(MeetingRoomId $meetingRoomId): MeetingRoom;

    public function all(): array;
}
