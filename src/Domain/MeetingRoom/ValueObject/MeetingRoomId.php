<?php

namespace App\Domain\MeetingRoom\ValueObject;

use App\Domain\Common\ValueObject\AggregateRootId;

class MeetingRoomId extends AggregateRootId
{
}
