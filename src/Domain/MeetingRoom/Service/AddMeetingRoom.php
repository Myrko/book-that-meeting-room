<?php

namespace App\Domain\MeetingRoom\Service;

use App\Domain\MeetingRoom\Aggregate\MeetingRoom;
use App\Domain\MeetingRoom\Repository\MeetingRoomRepositoryInterface;

class AddMeetingRoom
{
    private $meetingRoomRepository;

    public function __construct(MeetingRoomRepositoryInterface $meetingRoomRepository)
    {
        $this->meetingRoomRepository = $meetingRoomRepository;
    }

    public function __invoke(string $alias)
    {
       $meetingRoom = MeetingRoom::new($alias);

       $this->meetingRoomRepository->add($meetingRoom);

       return (string) $meetingRoom->id();
    }
}
