<?php

return [
    'Symfony\Bundle\FrameworkBundle\FrameworkBundle' => ['all' => true],
    'Symfony\Bundle\WebServerBundle\WebServerBundle' => ['dev' => true],
    'League\Tactician\Bundle\TacticianBundle' => ['all' => true],
    'Doctrine\Bundle\DoctrineCacheBundle\DoctrineCacheBundle' => ['all' => true],
    'Doctrine\Bundle\DoctrineBundle\DoctrineBundle' => ['all' => true],
    'Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle' => ['all' => true],
];
